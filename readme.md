# weak-memoize-unary

Memoize a function that takes a single argument using a weak map.

## Usage

```ts
import { weakMemoizeUnary } from 'weak-memoize-unary'
```

### weakMemoizeUnary

Memoize a function that takes a single argument using a weak map.

Parameters:

* `fn: (key: TArg) => TResult`: The function to memoize. Must be an object.

Return value:

* `(key: TArg) => TResult`: The memoized function
