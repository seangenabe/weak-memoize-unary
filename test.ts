import test from 'ava'
import { weakMemoizeUnary } from '.';

test('basic tests', t => {
  const a = {}
  const b = {}
  const f = () => ({ z: 1 })
  const g = weakMemoizeUnary(f)
  const c = g(a)
  t.is(g(a), c)
  const d = g(b)
  t.is(g(b), d)
  t.not(c, d)
  t.is(c.z, 1)
  t.is(d.z, 1)
  c.z = 2
  t.is(c.z, 2)
  t.is(d.z, 1)
  t.throws(() => g(null as any))
  t.throws(() => g(undefined as any))
  t.throws(() => g(1 as any))
  t.throws(() => g("hello" as any))
  t.throws(() => g(Symbol() as any))
})
